package com.example.lab2.controller;

import com.example.lab2.model.Book;
import com.example.lab2.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Map;

@Controller
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping(value = "/book-list")
    public String  getBooksList(Model model) {
        model.addAttribute("books", bookService.getAllBooks());
        return "home-page";
    }

    @PostMapping(value = "/add")
    public String  addNewBook(
            @ModelAttribute("book") Book book,
            Model model
    ) {
        try {
            bookService.addNewBook(book);
        } catch (RuntimeException exception) {
            model.addAttribute("exceptionMessage", exception.getMessage());
            model.addAttribute("books", bookService.getAllBooks());
            return "home-page";
        }
        return "redirect:book-list";
    }
}
