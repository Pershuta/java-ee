package com.example.lab2.repository;

import com.example.lab2.model.Book;
import com.sun.istack.internal.NotNull;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class BookRepository {

    private static final Map<String, Book> database = initDatabase();

    public void addNewBook(@NotNull final Book book){
        database.put(book.getIsbn(), book);
    }

//    public Book getByIsbn(@NotNull final String isbn){
//        return dataBase.get(isbn);
//    }

    public boolean containsIsbn(@NotNull final String isbn){
        if(database.containsKey(isbn))
            return true;
        return false;
    }
    public Collection<Book> getAllBooks(){
        return database.values();
    }

    private static Map<String, Book> initDatabase() {
        HashMap<String, Book> map = new HashMap<>();
        map.put("1", new Book("book1","1","author1"));
        map.put("2", new Book("book2","2","author2"));
        map.put("3", new Book("book3","3","author3"));
        return map;
    }
}
